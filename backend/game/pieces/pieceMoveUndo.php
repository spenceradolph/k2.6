<?php
session_start();
include("../../db.php");
$gameId = $_SESSION['gameId'];
$myTeam = $_SESSION['myTeam'];
$query = 'SELECT gameActive, gamePhase, gameCurrentTeam, gameBattleSection FROM GAMES WHERE gameId = ?';
$preparedQuery = $db->prepare($query);
$preparedQuery->bind_param("i", $gameId);
$preparedQuery->execute();
$results = $preparedQuery->get_result();
$r = $results->fetch_assoc();
$gamePhase = $r['gamePhase'];
$gameCurrentTeam = $r['gameCurrentTeam'];
$gameBattleSection = $r['gameBattleSection'];
if ($r['gameActive'] != 1) {
    header("location:home.php?err=1");
    exit;
}
if ($myTeam != $gameCurrentTeam) {
    echo "Cannot undo, not your team's turn.";
    exit;
}
if ($gamePhase != 2 && $gamePhase != 3 && $gamePhase != 4) {
    echo "Cannot undo during this phase.";
    exit;
}
if ($gameBattleSection != "none") {
    echo "Cannot undo during battle";
    exit;
}
$query = 'SELECT movementId, movementFromPosition, movementFromContainer, movementNowPlacement FROM movements WHERE movementGameId = ? ORDER BY movementId DESC LIMIT 0, 1';
$query = $db->prepare($query);
$query->bind_param("i", $gameId);
$query->execute();
$results = $query->get_result();
$num_results = $results->num_rows;
if ($num_results > 0) {
    $r = $results->fetch_assoc();
    $movementId = $r['movementId'];
    $movementFromPosition = $r['movementFromPosition'];
    $movementFromContainer = $r['movementFromContainer'];
    $movementPlacementId = $r['movementNowPlacement'];
    
    $query = 'SELECT placementPositionId, placementVisible FROM placements WHERE placementId = ?';
    $query = $db->prepare($query);
    $query->bind_param("i", $movementPlacementId);
    $query->execute();
    $results = $query->get_result();
    $r = $results->fetch_assoc();
    $oldPositionId = $r['placementPositionId'];
    $placementVisible = $r['placementVisible'];
    
    $dist = 1;
    $query = 'UPDATE placements SET placementPositionId = ?, placementCurrentMoves = placementCurrentMoves + 1, placementContainerId = ? WHERE (placementId = ?)';
    if ($movementFromPosition == $oldPositionId) {
        $dist = 0;
        $query = 'UPDATE placements SET placementPositionId = ?, placementContainerId = ? WHERE (placementId = ?)';
    }
    
    $query = $db->prepare($query);
    $query->bind_param("iii", $movementFromPosition,  $movementFromContainer,  $movementPlacementId);
    $query->execute();
    $query = 'DELETE FROM movements WHERE movementId = ?';
    $query = $db->prepare($query);
    $query->bind_param("i", $movementId);
    $query->execute();
    $query = 'SELECT placementUnitId, placementCurrentMoves, placementBattleUsed FROM placements WHERE placementId = ?';
    $query = $db->prepare($query);
    $query->bind_param("i", $movementPlacementId);
    $query->execute();
    $results = $query->get_result();
    $r = $results->fetch_assoc();
    $placementUnitId = $r['placementUnitId'];
    $placementCurrentMoves = $r['placementCurrentMoves'];
    $placementBattleUsed = $r['placementBattleUsed'];
    $unitNames = ['Transport', 'Submarine', 'Destroyer', 'AircraftCarrier', 'ArmyCompany', 'ArtilleryBattery', 'TankPlatoon', 'MarinePlatoon', 'MarineConvoy', 'AttackHelo', 'SAM', 'FighterSquadron', 'BomberSquadron', 'StealthBomberSquadron', 'Tanker', 'LandBasedSeaMissile'];
    $battleUsedText = "";
    if ($placementBattleUsed == 1) {
        $battleUsedText = "\nUsed in Attack";
    }
    $newTitle = $unitNames[$placementUnitId]."\nMoves: ".$placementCurrentMoves.$battleUsedText;
    $updateType = "pieceMove";
    $query = 'INSERT INTO updates (updateGameId, updateType, updatePlacementId, updateNewPositionId, updateNewContainerId, updateHTML) VALUES (?, ?, ?, ?, ?, ?)';
    $query = $db->prepare($query);
    $query->bind_param("isiiis", $gameId, $updateType, $movementPlacementId, $movementFromPosition, $movementFromContainer, $newTitle);
    $query->execute();
    echo "Movement Undone.";

    //only to check if the piece is no longer seen by enemy pieces
    $newPositionId = $movementFromPosition;

    //TODO: This value would be optimized as the max of distance it can see others and it can be seen by others (plus 1)
    $maxVisibilityDistance = 1 + 1; //Whatever the longest distance to see something is (plus 1 to account for going out of range)

    $positionsToCheck = [];
    for ($j = 0; $j < sizeof($_SESSION['dist'][0]); $j++) {
        if ($_SESSION['dist'][$newPositionId][$j] <= $maxVisibilityDistance) {
            array_push($positionsToCheck, $j);
        }
    }

    $newplacementVisible = 0;  //how many things are seeing it right now
    $wasAnUpdate = 0;
    for ($x = 0; $x < sizeof($positionsToCheck); $x++) {
        $currentPosition = $positionsToCheck[$x];
        $distanceToCurrentPosition = (int) $_SESSION['dist'][$newPositionId][$currentPosition];
        $distanceToOldPosition = (int) $_SESSION['dist'][$oldPositionId][$currentPosition];

        $query = 'SELECT placementUnitId FROM placements WHERE (placementPositionId = ?) AND (placementContainerId = -1) AND (placementTeamId != ?) AND (placementGameId = ?)';
        $query = $db->prepare($query);
        $query->bind_param("isi", $currentPosition, $myTeam, $gameId);
        $query->execute();
        $results = $query->get_result();
        $num_results = $results->num_rows;
        for ($k = 0; $k < $num_results; $k++) {
            $r = $results->fetch_assoc();
            $enemyPlacementUnitId = $r['placementUnitId'];

            if ($_SESSION['visibility'][$enemyPlacementUnitId][$placementUnitId] >= $distanceToCurrentPosition) {
                $newplacementVisible += 1;
            }
        }
    }

    //if there was a change from old to new for the piece that moved
    if (($placementVisible == 0 && $newplacementVisible > 0) || ($placementVisible > 0 && $newplacementVisible == 0)) {
        $query = 'UPDATE placements SET placementVisible = ? WHERE placementId = ? OR placementContainerId = ?';
        $query = $db->prepare($query);
        $query->bind_param("iii", $newplacementVisible, $placementId, $placementId);
        $query->execute();

        if ($newplacementVisible == 0) {
            $updateType = "visRemove";
            $query = 'INSERT INTO updates (updateGameId, updateType, updatePlacementId) VALUES (?, ?, ?)';
            $query = $db->prepare($query);
            $query->bind_param("isi", $gameId, $updateType, $placementId);
            $query->execute();
        } else {
            $updateHTML = "";
            $unitNames = ['Transport', 'Submarine', 'Destroyer', 'AircraftCarrier', 'ArmyCompany', 'ArtilleryBattery', 'TankPlatoon', 'MarinePlatoon', 'MarineConvoy', 'AttackHelo', 'SAM', 'FighterSquadron', 'BomberSquadron', 'StealthBomberSquadron', 'Tanker', 'LandBasedSeaMissile'];
            $placementIdE = $placementId;
            $placementUnitIdE = $placementUnitId;
            $placementTeamIdE = $myTeam;
            $placementCurrentMovesE = $placementCurrentMoves;
            $placementBattleUsedE = $placementBattleUsed;
            $pieceFunctions = ' draggable="true" ondragstart="pieceDragstart(event, this);" ondragleave="pieceDragleave(event, this);" onclick="pieceClick(event, this);" ondragenter="pieceDragenter(event, this);" ';
            $containerFunctions = " ondragenter='containerDragenter(event, this);' ondragleave='containerDragleave(event, this);' ondragover='positionDragover(event, this);' ondrop='positionDrop(event, this);' ";
            $battleUsedText = "";
            if ($placementBattleUsedE == 1) {
                $battleUsedText = "\nUsed in Attack";
            }
            $updateHTML = $updateHTML."<div class='".$unitNames[$placementUnitIdE]." gamePiece ".$placementTeamIdE."' title='".$unitNames[$placementUnitIdE]."\nMoves: ".$placementCurrentMovesE.$battleUsedText."' data-placementId='".$placementIdE."' ".$pieceFunctions.">";
            if ($placementUnitIdE == 0 || $placementUnitIdE == 3) {
                if ($placementUnitIdE == 0) {
                    $classthing = "transportContainer";
                } else {
                    $classthing = "aircraftCarrierContainer";
                }
                $updateHTML = $updateHTML."<div class='".$classthing."' data-positionId='-1' ".$containerFunctions.">";  //open the container
                $query2 = 'SELECT placementId, placementUnitId, placementCurrentMoves, placementBattleUsed FROM placements WHERE (placementGameId = ?) AND (placementContainerId = ?)';
                $query2 = $db->prepare($query2);
                $query2->bind_param("ii", $gameId, $placementIdE);
                $query2->execute();
                $results2 = $query2->get_result();
                $num_results2 = $results2->num_rows;
                for ($b = 0; $b < $num_results2; $b++) {
                    $x = $results2->fetch_assoc();
                    $placementId2 = $x['placementId'];
                    $placementUnitId2 = $x['placementUnitId'];
                    $placementCurrentMoves2 = $x['placementCurrentMoves'];
                    $battleUsedText2 = "";
                    if ($x['placementBattleUsed'] == 1) {
                        $battleUsedText2 = "\nUsed in Attack";
                    }
                    $updateHTML = $updateHTML."<div class='".$unitNames[$placementUnitId2]." gamePiece ".$placementTeamIdE."' title='".$unitNames[$placementUnitId2]."\nMoves: ".$placementCurrentMoves2.$battleUsedText2."' data-placementId='".$placementId2."' ".$pieceFunctions."></div>";
                }
                $updateHTML = $updateHTML."</div>";  //end the container
            }
            $updateHTML = $updateHTML."</div>";  //end the overall piece

            $updateType = "visAdd";
            $query = 'INSERT INTO updates (updateGameId, updateType, updatePlacementId, updateNewPositionId, updateHTML) VALUES (?, ?, ?, ?, ?)';
            $query = $db->prepare($query);
            $query->bind_param("isiis", $gameId, $updateType, $placementId, $movementFromPosition, $updateHTML);
            $query->execute();
        }
    }
    exit;
} else {
    echo "No more undo's can be made.";
    exit;
}
